CREATE TABLE authors (
  id INTEGER AUTO_INCREMENT PRIMARY KEY,
  first_name VARCHAR(50) NOT NULL,
  second_name VARCHAR(50) NOT NULL,
  INDEX (second_name)
);

CREATE TABLE books (
  id INTEGER AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(150) NOT NULL,
  INDEX (name)
);

CREATE TABLE authors_books (
  author_id INTEGER NOT NULL,
  book_id INTEGER NOT NULL,
  FOREIGN KEY (author_id) REFERENCES authors(id),
  FOREIGN KEY (book_id) REFERENCES books(id)
);

INSERT INTO authors (id, first_name, second_name) VALUES (1, 'Чарльз','Диккенс');
INSERT INTO authors (id, first_name, second_name) VALUES (2, 'Сомерсет','Моэм');
INSERT INTO authors (id, first_name, second_name) VALUES (3, 'Эрих','Ремарк');
INSERT INTO authors (id, first_name, second_name) VALUES (4, 'Борис','Пастернак');

INSERT INTO books (id, name) VALUES (1, 'Война и мир');
INSERT INTO books (id, name) VALUES (2, 'Мастер и маргарита');
INSERT INTO books (id, name) VALUES (3, 'Сто лет одиночества');
INSERT INTO books (id, name) VALUES (4, 'Граф Монтекристо');
INSERT INTO books (id, name) VALUES (5, 'Эра милосердия');
INSERT INTO books (id, name) VALUES (6, 'Честь имею');

-- Книга написана одним автором
INSERT INTO authors_books (author_id, book_id) VALUES (1,1);

-- Книга написана двумя авторами
INSERT INTO authors_books (author_id, book_id) VALUES (1,2);
INSERT INTO authors_books (author_id, book_id) VALUES (2,2);

-- Книга написана тремя авторами
INSERT INTO authors_books (author_id, book_id) VALUES (1,3);
INSERT INTO authors_books (author_id, book_id) VALUES (2,3);
INSERT INTO authors_books (author_id, book_id) VALUES (3,3);
INSERT INTO authors_books (author_id, book_id) VALUES (2,4);
INSERT INTO authors_books (author_id, book_id) VALUES (3,4);
INSERT INTO authors_books (author_id, book_id) VALUES (4,4);

-- Книга написана четырьмя авторами
INSERT INTO authors_books (author_id, book_id) VALUES (1,5);
INSERT INTO authors_books (author_id, book_id) VALUES (2,5);
INSERT INTO authors_books (author_id, book_id) VALUES (3,5);
INSERT INTO authors_books (author_id, book_id) VALUES (4,5);

SELECT books.id, books.name, count(authors_books.author_id) as author_count
FROM authors_books LEFT JOIN books ON books.id = authors_books.book_id
GROUP BY books.id
HAVING count(authors_books.author_id) = 3;