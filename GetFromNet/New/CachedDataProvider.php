<?php

namespace src\Decorator;

use DateTime;
use Exception;
use Psr\Cache\CacheItemInterface;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Cache\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use src\Integration\DataProviderInterface;

class CachedDataProvider implements DataProviderInterface
{
	const CACHE_EXPIRE_MODIFY = '+1 day';
	private $cache;
	private $dataProvider;
	private $logger;

	/**
	 * @param CacheItemPoolInterface $cache
	 */
	public function __construct(
		DataProviderInterface $dataProvider,
		CacheItemPoolInterface $cache,
		LoggerInterface $logger
	)
	{
		$this->dataProvider = $dataProvider;
		$this->cache = $cache;
		$this->logger = $logger;
	}

	/**
	 * {@inheritdoc}
	 * @throws InvalidArgumentException
	 */
	public function get(string $host, string $user, string $password, array $request): array
	{
		try {
			$cacheItem = $this->getElementFromCache($request);
			if ($cacheItem->isHit()) return $cacheItem->get();
		} catch (Exception $exception) {
			$this->logger->critical(
				"Can\'t get data from cache server. File " . $exception->getFile() . ". Error message:" . $exception->getMessage()
			);
		}

		$result = $this->dataProvider->get($host, $user, $password, $request);

		$this->saveCacheElement($cacheItem, $result);

		return $result;
	}

	private function getCacheKey(array $input)
	{
		return md5(json_encode($input));
	}

	/**
	 * @param string $cacheKey
	 * @return CacheItemInterface
	 * @throws Exception
	 * @throws InvalidArgumentException
	 */
	private function getElementFromCache(array $input)
	{
		$cacheKey = $this->getCacheKey($input);
		try {
			return $this->cache->getItem($cacheKey);
		} catch (InvalidArgumentException $exception) {
			$this->logger->critical(
				"There is error in cache key $cacheKey. File " . $exception->getFile() . ". Error message:" . $exception->getMessage()
			);
			throw $exception;
		}
	}

	private function saveCacheElement(CacheItemInterface $cacheItem, array $result): void
	{
		try {
			if (!empty($result)) {
				$cacheItem->set($result)->expiresAt((new DateTime())->modify(self::CACHE_EXPIRE_MODIFY));
				$this->cache->save($cacheItem);
			}
		} catch (Exception $exception) {
			$this->logger->critical(
				"Can\'t save element {$cacheItem->getKey()} to cache server. File " . $exception->getFile() . ". Error message:" . $exception->getMessage()
			);
		}
	}
}
