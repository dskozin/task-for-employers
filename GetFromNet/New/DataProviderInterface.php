<?php
declare(strict_types=1);

namespace src\Integration;

interface DataProviderInterface
{
	public function get(string $host, string $user, string $password, array $request): array;
}