<?php

namespace src\Integration;

use Psr\Log\LoggerInterface;

class DataProvider implements DataProviderInterface
{
	protected $logger;

	/**
	 * @param $host
	 * @param $user
	 * @param $password
	 */
	public function __construct(LoggerInterface $logger)
	{
		$this->logger = $logger;
	}

	/**
	 * @param string $host
	 * @param string $user
	 * @param string $password
	 * @param array $request
	 */
	public function get(string $host, string $user, string $password, array $request): array
	{
		try{
			// returns a response from external service
			return ['response'];
		} catch (\Exception $exception){
			$this->logger->critical(
				"Can\'t get data from remote server. File " . $exception->getFile() . ". Error message:" . $exception->getMessage()
			);
			return [];
		}
	}
}
