<?php

namespace src\Integration;

class DataProvider
{
	private $host;
	private $user;
	private $password;

	/**
	 * @param $host
	 * @param $user
	 * @param $password
	 */
	public function __construct($host, $user, $password)
	{
		$this->host = $host;
		$this->user = $user;
		$this->password = $password;
	}

	/**
	 * @param array $request
	 *
	 * @return array
	 */
	/*
	 * Думаю что перехват ошибок должен осуществляться еще на этом этапе.
	 */
	public function get(array $request)
	{
		// returns a response from external service
	}
}
