<?php
declare(strict_types=1);

/*
 * Для того что бы избежать ошибок при параллельном доступе к файлу
 * следует использовать блокировки примерно следующим образом.
 * Очередной запущенный экземпляр скрипта будет ожидать на flock
 * освобождения файла.
 */
$path = "./counter.txt";
$tmp = fopen($path, 'rb');
@flock($tmp, LOCK_SH);

file_put_contents($path, file_get_contents($path) + 1);

@flock($tmp, LOCK_UN);
fclose($tmp);