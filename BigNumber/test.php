<?php
declare(strict_types=1);

include_once 'BigNumbersSummator.php';

$summator = new SkyEng\BigNumbersSummator();

echo $summator->sum('39', '2') . PHP_EOL;
echo $summator->sum('389', '2') . PHP_EOL;
echo $summator->sum('35', '35') . PHP_EOL;
echo $summator->sum('5', '5') . PHP_EOL;
echo $summator->sum('12341234', '62348294') . PHP_EOL;
echo $summator->sum('asd1234asd1234', 'asd62348eaewa294') . PHP_EOL;
echo $summator->sum('12341234234523', '6234829423452345243523452345') . PHP_EOL;
echo $summator->sumMany('62341234234523', '32312341', '12341234sdfgsdg1734', '1234234123') . PHP_EOL;
