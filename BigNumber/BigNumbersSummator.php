<?php
declare(strict_types=1);

namespace SkyEng;

class BigNumbersSummator
{
	private $longArgument;
	private $shortArgument;

	/**
	 * @param string $firstArg
	 * @param string $secondArg
	 * @return string
	 * @throws \Exception
	 */
	public function sum(string $firstArg, string $secondArg)
	{
		$this->prepareArgs($firstArg, $secondArg);

		return $this->summing();
	}

	/**
	 * @param string ...$arguments
	 * @return string
	 * @throws \Exception
	 */
	public function sumMany(string ...$arguments): string
	{
		$sum = '0';
		if (count($arguments) === 0) return $sum;

		foreach ($arguments as $argument) {
			$sum = $this->sum($sum, $argument);
		}

		return $sum;
	}

	/**
	 * @param string $firstArg
	 * @param string $secondArg
	 * @return string
	 * @throws \Exception
	 */
	private function summing(): string
	{
		$sum = '';
		$i = 0;
		$flag = 0;

		while (true) {
			$digitSum = $this->longArgument[$i] + $this->shortArgument[$i] + $flag;
			$flag = $digitSum > 9 ? 1 : 0;
			$sum .= $digitSum % 10;
			$i++;

			// когда заканчивается короткий аргумент
			// добиваем последнее значение и копируем остальную часть строки
			if (!isset($this->shortArgument[$i])) {
				if (!isset($this->longArgument[$i])) {
					$sum .= $flag === 0 ? '' : '1';
				} else {
					$sum .= $this->longArgument[$i] + $flag;
					$sum .= mb_substr($this->longArgument, $i + 1);
				}
				break;
			}
		}

		return strrev($sum);
	}

	/**
	 * @param string $firstArg
	 * @param string $secondArg
	 * @throws \Exception
	 */
	private function prepareArgs(string $firstArg, string $secondArg)
	{
		$firstArg = $this->prepareArg($firstArg);
		$secondArg = $this->prepareArg($secondArg);

		if (mb_strlen($firstArg) > mb_strlen($secondArg)) {
			$this->longArgument = $firstArg;
			$this->shortArgument = $secondArg;
		} else {
			$this->longArgument = $secondArg;
			$this->shortArgument = $firstArg;
		}
	}

	/**
	 * @param string $arg
	 * @return string
	 * @throws \Exception
	 */
	private function prepareArg(string $arg): string
	{
		$arg = strrev(preg_replace('#[\D]*#', '', $arg));

		if (strlen($arg) === 0 || $arg === null)
			throw new \Exception("No! Arguments is not correct!");

		return $arg;
	}
}